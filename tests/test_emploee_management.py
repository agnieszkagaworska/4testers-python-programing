from src.employee_management import get_random_employee_record


def test_generated_seniority_years_are_in_proper_range():
    test_employee = get_random_employee_record()
    employee_seniority_years = test_employee['seniority_years']
    assert employee_seniority_years >= 0
    assert employee_seniority_years <= 40


def test_single_employee_generated_has_proper_keys():
    test_employee = get_random_employee_record()
    assert 'email' in test_employee.keys()
    assert 'seniority_years' in test_employee.keys()
    assert 'female' in test_employee.keys()
