character_1 = {
    "nick" : "maestro_54",
    "type" : "warrior",
    "exp_points" : 3000
}

character_2 = {
    "nick" : "kaczuszka_57",
    "type" : "tank",
    "exp_points" : 5000
}


def display_character_description(character):
    print(f"The player {character['nick']} is of type {character['type']} and has {character['exp_points']} EXP")



    display_character_description(character_1)
    display_character_description(character_2)

addresses = [
    {
        "city": "Krakow",
        "street": "Basztowa",
        "house_number": 5,
        "post_code": "31-097"
    },
    {
        "city": "Wroclaw",
        "street": "Dluga",
        "house_number": 10,
        "post_code": "43-397"
    },
    {
        "city": "Gdansk",
        "street": "Szeroka",
        "house_number": 57,
        "post_code": "87-074"
    }
    ]

print()