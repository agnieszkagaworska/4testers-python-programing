animals = [
    {'name': 'Burek', 'kind': 'dog', 'age': 7},
    {'name': 'Bonifacy', 'kind': 'cat', 'age': None},
    {'name': 'Misio', 'kind': 'hamster', 'age': 1},
]

addresses = [
    {'city': 'Warszawa', 'street': 'Jemiołowa', 'house_number': 4, 'post_code': '11-510'},
    {'city': 'Wrocław', 'street': 'Skwierzyńska', 'house_number': 100, 'post_code': '53-521'},
    {'city': 'Giżycko', 'street': 'Słoneczna', 'house_number': 7, 'post_code': '11-500'},
]


if __name__ == '__main__':
    print(animals[-1]['name'])
    animals[1]['age'] = 2
    print(animals[1])
    animals.insert(0, {'name': 'Reksio', 'kind': 'dog', 'age': 9})
    print(animals)

    #Zadanie
    print(addresses[2]['post_code'])
    print(addresses[1]['city'])
    addresses[0]['city'] = 'Kraków'
    print(addresses)
