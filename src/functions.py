def print_word_uppercase(word):
    uppercase_word = word.upper()
    print(uppercase_word)


if __name__ == '__main__':
    print_word_uppercase("piesek")
    print_word_uppercase("adam")
    print_word_uppercase("kotek")


