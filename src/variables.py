first_name = "Adrian"
last_name = "Gonciarz"
age = 34

print(first_name)
print(last_name)
print(age)

# Below I describe my best friend
friend_name = "Michal"
friend_age = 42
friend_number_of_pets = 1
friend_has_driving_licence = True
friendship_duration_in_years = 18.5

print("Friend's name:", friend_name, sep='\t')

print(friend_name, friend_age, friend_number_of_pets, friend_has_driving_licence, friendship_duration_in_years, sep=',')
