import random
from datetime import date

female_fnames = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
male_fnames = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']

MALE = 0
FEMALE = 1


def generate_random_personal_record(gender):
    if gender == MALE:
        new_firstname = random.choice(male_fnames)
    elif gender == FEMALE:
        new_firstname = random.choice(female_fnames)

    new_lastname = random.choice(surnames)

    new_email = f'{new_firstname.lower()}.{new_lastname.lower()}@example.com'

    new_age = random.randint(5, 45)

    if new_age >= 18:
        is_adult = True
    else:
        is_adult = False

    current_year = date.today().year
    born_year = current_year - new_age

    personal_data = {
        "firstname": new_firstname,
        "lastname": new_lastname,
        "country": random.choice(countries),
        "email": new_email,
        "age": new_age,
        "adult": is_adult,
        "birth_year": born_year
    }
    return personal_data


def generate_list_of_females_and_males(number_of_females, number_of_males):
    list_of_people = []
    for _ in range(number_of_females):
        list_of_people.append(generate_random_personal_record(FEMALE))
    for _ in range(number_of_males):
        list_of_people.append(generate_random_personal_record(MALE))
    return list_of_people


def print_introductions_for_list_of_people(people):
    for person in people:
        print(f'Hi! I am {person["firstname"]} {person["lastname"]}. I come from {person["country"]} and I was born in {person["birth_year"]}')


if __name__ == '__main__':
    list_of_10_people = generate_list_of_females_and_males(5, 5)
    print_introductions_for_list_of_people(list_of_10_people)