import uuid

def calculate_average_of_list_numbers(input_list):
    return sum(input_list) / len(input_list)

def calculate_average_of_two_numbers(x, y):
    return (x + y) / 2

def generate_random_login_input(email):
    login = {
        "email": email,
        "password": str(uuid.uuid4())
    }

    return login


if __name__ == '__main__':
    january = [-4, 1.0, -7, 2]
    february = [-13, -9, -3, 3]
    january_average = calculate_average_of_list_numbers(january)
    february_average = calculate_average_of_list_numbers(february)
    bimonthly_average = calculate_average_of_two_numbers(january_average, february_average)
    print(bimonthly_average)
    print(generate_random_login_input("wiola@cokolwiek.com"))

# Zadanie 2 #




