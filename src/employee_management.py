import random
import time


def generate_random_email():
    domain = "example.com"
    names = ["john", "jack", "carol", "alex", "marzena"]
    random_name = random.choice(names)
    suffix = time.time()
    return f'{random_name}.{suffix}@{domain}'


def get_random_employee_record():
    record = {
        "email": generate_random_email(),
        "seniority_years": random.randint(0, 40),
        "female": random.choice([False, True])
    }
    return record


def generate_number_of_employee_records(n):
    employees = []
    for _ in range(n):
        employees.append(get_random_employee_record())
    return employees


if __name__ == '__main__':
    print(generate_number_of_employee_records(10))
