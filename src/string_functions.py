def print_greetings_for_a_person_in_the_city(person_name, city):
    print(f"Witaj {person_name}! Miło Cię widziec w naszym mieście: {city}!")


def generate_email_for_4testers(first_name, last_name):
   print(f"{first_name.lower()}.{last_name.lower()}@4testers.pl")


if __name__ == '__main__':
    print_greetings_for_a_person_in_the_city("Ania", "Wrocław")
    print_greetings_for_a_person_in_the_city("Marek", "Gdańsk")
    print(generate_email_for_4testers("Janusz", "Nowak"))
    print(generate_email_for_4testers("Barbara", "Kowalska"))
